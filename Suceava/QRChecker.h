//
//  QRChecker.h
//  Suceava
//
//  Created by Mert Aydoğan on 06.11.2023.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QRChecker : NSObject

- (void)startBackgroundTask;
@end

NS_ASSUME_NONNULL_END
