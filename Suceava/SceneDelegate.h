//
//  SceneDelegate.h
//  Suceava
//
//  Created by Mert Aydoğan on 05.11.2023.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

